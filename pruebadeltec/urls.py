from django.conf.urls import url, include
from django.contrib import admin
from pruebadeltec.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', Index.as_view(), name='index'),

    url(r'^',
        include('app.bodega.urls', namespace='bodega')),
]
