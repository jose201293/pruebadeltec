from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms import ModelForm
from app.bodega.models import *
# CRIPY-FORMS
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Div, Field, HTML, ButtonHolder
from crispy_forms.bootstrap import FormActions, StrictButton, PrependedText, InlineRadios, AppendedText
from django.contrib.admin.widgets import FilteredSelectMultiple


class FormularioPersona(ModelForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-group'
    helper.layout = Layout(
        Div(
            Div(Field('identificacion', css_class='form-control'),
                css_class='col-lg-6'), css_class='col-lg-12'
        ),

        Div(

            Div(Field('nombres', css_class='form-control'),
                css_class='col-lg-6'),
            Div(Field('apellidos', css_class='form-control'),
                css_class='col-lg-6'),

            css_class='col-lg-12'
        ),
        Div(
            HTML('<hr>'),
            css_class='col-md-12'
        ),

        Div(HTML('<center>'),
            Div(ButtonHolder(
                # HTML(
                #     '<a class="btn btn-default" href={% url "administrador:lis-emp" %}><span class="glyphicon glyphicon-chevron-left"></span> Cancelar</a>'),
                HTML(
                    '<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Guardar </button>')
            ),
        ),
            HTML('<br>'),
            HTML('</center>'),
            css_class='col-md-12'
        ),
    )

    class Meta:
        model = Persona
        fields = ['identificacion', 'nombres', 'apellidos']

    def __init__(self, *args, **kwargs):
        super(FormularioPersona, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(FormularioPersona, self).clean()
        return cleaned_data


class FormularioRecurso(forms.ModelForm):
    # TODO: Define other fields here
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-group'
    helper.layout = Layout(

        Div(
            Div(Field('codigo', css_class='form-control'), css_class='col-lg-6'),
            Div(Field('serie', css_class='input-sm'), css_class='col-lg-6'),
            css_class='col-lg-12 panel w3-border w3-border-green',

        ),

        Div(
            Div(Field('nombre', css_class='form-control'), css_class='col-lg-6'),
            Div(Field('categoria', css_class='input-sm'), css_class='col-lg-6'),
            Div(Field('marca', css_class='input-sm'), css_class='col-lg-6'),

            css_class='col-lg-12 panel w3-border w3-border-green',
        ),

        Div(
            HTML('<hr>'),
            css_class='col-md-12'
        ),
        Div(HTML('<center>'),
            Div(ButtonHolder(
                HTML(
                    '<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Guardar </button>')
            ),
        ),
            HTML('<br>'),
            HTML('</center>'),
            css_class='col-md-12'
        ),
    )

    class Meta:
        model = Recurso
        fields = ['codigo', 'nombre', 'categoria', 'marca', 'serie']

    def __init__(self, *args, **kwargs):
        super(FormularioRecurso, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(FormularioRecurso, self).clean()
        return cleaned_data


class FormularioAsignacion(forms.Form):

    recursos = forms.ModelMultipleChoiceField(queryset=Recurso.objects.filter(
        estado='Sin asignar'), widget=forms.SelectMultiple(attrs={'class': 'form-control'}))

    personas = forms.ModelChoiceField(queryset=Persona.objects.all(),widget=forms.Select(attrs={'class': 'form-control'}))
#
class FormularioTrasladoRecursos(forms.Form):
    
    recursos = forms.ModelMultipleChoiceField(queryset=Recurso.objects.filter(
        estado='Sin asignar'), widget=forms.SelectMultiple(attrs={'class': 'form-control'}))

    personas = forms.ModelChoiceField(queryset=Persona.objects.all(),widget=forms.Select(attrs={'class': 'form-control'}))
