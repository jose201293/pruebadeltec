from django.contrib import admin
from app.bodega.models import *
import datetime

admin.site.register(Persona)
admin.site.register(Recurso)
admin.site.register(Asignacion)
admin.site.register(Historial)
