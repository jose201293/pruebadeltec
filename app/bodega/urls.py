from django.conf.urls import url
from django.contrib import admin
from app.bodega.views import *

urlpatterns = [

    # URLS GESTION DE PERSONAS
    url(r'^ListarPersonas/$', ListarPersonas.as_view(), name='lis-per'),
    url(r'^CrearPersona/$', CrearPersona.as_view(), name='cre-per'),
    url(r'^ActualizarPersona/(?P<pk>[0-9]+)/$',
        ActualizarPersona.as_view(), name='act-per'),
    url(r'^EliminarPersona/(?P<pk>[0-9]+)/$',
        EliminarPersona.as_view(), name='elm-per'),

    # URLS GESTION DE RECURSOS
    url(r'^ListarRecursos/$', ListarRecursos.as_view(), name='lis-rec'),
    url(r'^CrearRecurso/$', CrearRecurso.as_view(), name='cre-rec'),
    url(r'^ActualizarRecurso/(?P<pk>[0-9]+)/$',
        ActualizarRecurso.as_view(), name='act-rec'),
    url(r'^EliminarRecurso/(?P<pk>[0-9]+)/$',
        EliminarRecurso.as_view(), name='elm-rec'),

    # URLS GESTION DE ASIGNACIONES
    url(r'^ListarAsignaciones/$', ListarAsignaciones.as_view(), name='lis-asi'),
    url(r'^ListarPersonaRecursos/$',
        ListarPersonaRecursos.as_view(), name='lis-per-rec'),
    url(r'^CrearAsignacion/$', CrearAsignacion.as_view(), name='cre-asi'),
    url(r'^DetalleAsignacion/(?P<pk>[0-9]+)/$',
        DetalleAsignacion.as_view(), name='det-asi'),

    url(r'^TrasladarRecursos/(?P<pk>[0-9]+)/$',
        TrasladarRecursos.as_view(), name='tra-rec'),

]
