from django.shortcuts import render
from app.bodega.models import *
from app.bodega.forms import *
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, ListView, DeleteView, DetailView
from django.shortcuts import render, redirect, get_object_or_404


# VISTA BASADA EN CLASE PARA LISTAR LAS PERSONAS

class ListarPersonas(ListView):
    model = Persona
    context_object_name = 'lista'
    template_name = 'persona/ListarPersonas.html'

# VISTA BASADA EN CLASE PARA CREAR PERSONA


class CrearPersona(CreateView):
    model = Persona
    form_class = FormularioPersona
    success_url = reverse_lazy('bodega:lis-per')
    template_name = 'persona/CrearPersona.html'

# VISTA BASADA EN CLASE PARA MODIFICAR DATOS DE LA PERSONA


class ActualizarPersona(UpdateView):
    model = Persona
    form_class = FormularioPersona
    success_url = reverse_lazy('bodega:lis-per')
    template_name = 'persona/ModificarPersona.html'


# VISTA BASADA EN CLASE PARA ELIMINAR UNA PERSONA

class EliminarPersona(DeleteView):
    model = Persona
    success_url = reverse_lazy('bodega:lis-per')
    template_name = 'persona/EliminarPersona.html'

    def get_context_data(self, **kwargs):
        context = super(EliminarPersona, self).get_context_data(**kwargs)
        asignacion = Asignacion.objects.filter(
            id_persona=self.kwargs['pk']).exists()
        if asignacion:
            if 'msj' not in context:
                context['msj'] = 'No se puede eliminar esta \
                persona porque tiene recursos asignados'
        return context

    def post(self, request, *args, **kwargs):
        if Asignacion.objects.filter(id_persona=self.kwargs['pk']).exists():
            return redirect('/ListarPersonas')
        else:
            super().delete(*args, request, **kwargs)
            return redirect('/ListarPersonas')


# VISTA BASADA EN CLASE PARA LISTAR LOS RECURSOS

class ListarRecursos(ListView):
    model = Recurso
    context_object_name = 'lista'
    template_name = 'recurso/ListarRecursos.html'

# VISTA BASADA EN CLASE PARA CREAR UN RECURSOS


class CrearRecurso(CreateView):
    model = Recurso
    form_class = FormularioRecurso
    success_url = reverse_lazy('bodega:lis-rec')
    template_name = 'recurso/CrearRecurso.html'

# VISTA BASADA EN CLASE PARA MODIFICAR EL RECURSO


class ActualizarRecurso(UpdateView):
    model = Recurso
    form_class = FormularioRecurso
    success_url = reverse_lazy('bodega:lis-rec')
    template_name = 'recurso/ModificarRecurso.html'

# VISTA BASADA EN CLASE PARA ELIMINAR UN RECURSO


class EliminarRecurso(DeleteView):
    model = Recurso
    success_url = reverse_lazy('bodega:lis-rec')
    template_name = 'recurso/EliminarRecurso.html'

    def get_context_data(self, **kwargs):
        context = super(EliminarRecurso, self).get_context_data(**kwargs)
        asignacion = Asignacion.objects.filter(
            id_recurso=self.kwargs['pk']).exists()
        if asignacion:
            if 'msj' not in context:
                context['msj'] = 'No se puede eliminar este \
                recurso ya ha sido asignado'
        return context

    def post(self, request, *args, **kwargs):
        if Asignacion.objects.filter(id_recurso=self.kwargs['pk']).exists():
            return redirect('/ListarRecursos')
        else:
            super().delete(*args, request, **kwargs)
            return redirect('/ListarRecursos')


# VISTA BASADA EN CLASE PARA LISTAR EL HISTORIAL DE ASIGNACIONES Y TRASLADOS

class ListarAsignaciones(ListView):
    context_object_name = 'lista'
    model = Historial
    template_name = 'asignaciones/ListarAsignaciones.html'

# VISTA BASADA EN CLASE PARA ASIGNAR RECURSOS A UNA PERSONA


class CrearAsignacion(TemplateView):
    form_class = FormularioAsignacion
    template_name = 'asignaciones/CrearAsignacion.html'

    def get_context_data(self, **kwargs):
        context = super(CrearAsignacion, self).get_context_data(**kwargs)
        # REALIZAMOS UNA CONSULTA PARA OBTENER TODOS LOS RECURSOS
        if 'lista_recursos' not in context:
            context['lista_recursos'] = Recurso.objects.all()

        if 'form' not in context:
            context['form'] = self.form_class
        return context

    # REESCRIBIMOS EL METODO POST PARA REALIZAR EL GUARDADO DE LOS DATOS
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            persona = request.POST.getlist('personas')
            recursos = request.POST.getlist('recursos')
            try:
                recursos = list(map(int, recursos))
                persona = list(map(int, persona))

            except:
                return render(request, self.template_name, {'msj': 'Error en los datos ', 'form': form})

            fecha = date.today()
            veri_asignados = verificar_recursos_asignados(
                recursos, persona)
            if veri_asignados:
                persona = get_object_or_404(Persona, pk=persona[0])
                for id_recurso in recursos:
                    recurso = get_object_or_404(Recurso, pk=id_recurso)
                    asignacion = Asignacion(
                        id_persona=persona,
                        id_recurso=recurso
                    )
                    historial = Historial(
                        id_persona=persona,
                        id_recurso=recurso,
                        evento = "Asignacion"
                    )
                    recurso.estado = "Asignado"
                    recurso.save()
                    asignacion.save()
                    historial.save()
                return redirect('/ListarPersonaRecursos/')
            else:
                return render(request, self.template_name, {'msj': 'Uno o mas de estos recursos ya fueron asignados a esta persona', 'form': form})
        else:
            return self.render_to_response(self.get_context_data(form=form))


# VISTA BASADA EN CLASE PARA LISTAR LAS PERSONAS CON RECURSOS ASIGNADOS

class ListarPersonaRecursos(ListView):
    model = Asignacion
    template_name = 'asignaciones/ListarPersonaRecursos.html'
    # query_can_prod = Asignacion.objects.raw('''SELECT id,id_persona_id FROM
    # bodega_asignacion GROUP BY id_persona_id  ''')

    def get_context_data(self, **kwargs):
        context = super(ListarPersonaRecursos, self).get_context_data(**kwargs)

        # REALIZAMOS UNA CONSULTA PARA OBTENER TODAS LAS PERSONAS QUE
        # TIENEN ASIGNADOS RECURSOS
        asignaciones = Asignacion.objects.all()
        lista_personas = []
        for asignacion in asignaciones:
            lista_personas.append(asignacion.id_persona_id)
        personas = Persona.objects.filter(id__in=lista_personas)

        if 'lista_personas' not in context:
            context['lista_personas'] = personas

        return context

# VISTA BASADA EN CLASES PARA DETALLAR LOS RECURSOS ASIGNADOS A UNA PERSONA


class DetalleAsignacion(TemplateView):
    model = Asignacion
    template_name = 'asignaciones/DetalleAsignacion.html'

    def get_context_data(self, **kwargs):
        context = super(DetalleAsignacion, self).get_context_data(**kwargs)
        # REALIZAMOS UNA CONSULTA PARA OBTENER TODOS LOS RECURSOS DE UNA
        # PERSONA
        if 'asignacion_list' not in context:
            context['asignacion_list'] = Asignacion.objects.filter(
                id_persona=self.kwargs['pk'])
        #
        if 'persona' not in context:
            persona = get_object_or_404(Persona, pk=self.kwargs['pk'])
            context['persona'] = persona.nombres + " " + persona.apellidos

        return context


class TrasladarRecursos(TemplateView):
    template_name = 'asignaciones/TrasladarRecursos.html'
    form_class = FormularioTrasladoRecursos

    def get_context_data(self, **kwargs):
        context = super(TrasladarRecursos, self).get_context_data(**kwargs)
        # REALIZAMOS UNA CONSULTA PARA OBTENER TODOS LOS RECURSOS DE UNA
        # PERSONA
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET or None)
            persona = get_object_or_404(Persona, pk=self.kwargs['pk'])
            asignaciones = Asignacion.objects.filter(
                id_persona=self.kwargs['pk'])
            lista_recursos = []
            for asignacion in asignaciones:
                lista_recursos.append(asignacion.id_recurso.id)
            recursos = Recurso.objects.filter(id__in=lista_recursos)
            personas = Persona.objects.all().exclude(id=self.kwargs['pk'])

            context['form'].fields['recursos'].queryset = recursos
            context['form'].fields['personas'].queryset = personas
            context['persona'] = persona.nombres + persona.apellidos

        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        persona = request.POST.getlist('personas')
        recursos = request.POST.getlist('recursos')
        if persona and recursos:
            try:
                recursos = list(map(int, recursos))
                persona = list(map(int, persona))

            except:
                return render(request, self.template_name, {'msj': 'Error en los datos ', 'form': form})
            veri_asignados = verificar_recursos_asignados(recursos, persona)
            if veri_asignados:
                persona = get_object_or_404(Persona, pk=persona[0])

                for id_rec in recursos:
                    asignacion = get_object_or_404(Asignacion, id_persona=self.kwargs[
                                                   'pk'], id_recurso=id_rec)
                    asignacion.id_persona = persona
                    asignacion.save()
                    historial = Historial(
                        id_persona=asignacion.id_persona,
                        id_recurso=asignacion.id_recurso,
                        evento = "Traslado"
                    )
                    asignacion.save()
                    historial.save()
                return redirect('/ListarPersonaRecursos/')

            else:
                return render(request, self.template_name, {'msj': 'Uno o mas de estos recursos ya fueron asignados a esta persona', 'form': form})

        else:
            return self.render_to_response(self.get_context_data(form=form))

# METODO PARA VERIFICAR SI ALGUNO DE LOS RECURSOS QUE SE INTENTAN ASIGNAR
# YA SE HAN REGISTRADO PREVIAMENTE


def verificar_recursos_asignados(recursos, persona):
    ban = True
    for recurso in recursos:
        if Asignacion.objects.filter(id_recurso=recurso, id_persona=persona[0]).exists():
            ban = False
            break
    return ban
