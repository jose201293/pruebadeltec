# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-08-26 23:50
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Asignacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_asignacion', models.DateField(default=datetime.date.today)),
            ],
            options={
                'ordering': ['fecha_asignacion'],
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identificacion', models.CharField(max_length=12, unique=True)),
                ('nombres', models.CharField(max_length=100)),
                ('apellidos', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ['nombres', 'apellidos'],
            },
        ),
        migrations.CreateModel(
            name='Recurso',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('categoria', models.CharField(choices=[('Tecnologia', 'Tecnologia'), ('Salud', 'Salud'), ('Educación', 'Educación'), ('Aseo', 'Aseo'), ('Alimentos', 'Alimentos'), ('otros', 'otros')], max_length=50)),
                ('codigo', models.CharField(max_length=100, unique=True)),
                ('nombre', models.CharField(max_length=100)),
                ('marca', models.CharField(max_length=100)),
                ('serie', models.CharField(max_length=100, unique=True)),
            ],
            options={
                'ordering': ['codigo', 'nombre', 'marca'],
            },
        ),
        migrations.AddField(
            model_name='asignacion',
            name='id_persona',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='bodega.Persona'),
        ),
        migrations.AddField(
            model_name='asignacion',
            name='id_recurso',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='bodega.Recurso'),
        ),
    ]
