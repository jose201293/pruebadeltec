from django.db import models
import datetime
from datetime import date
from django.core.validators import RegexValidator

# Create your models here.

class Persona(models.Model):
    identificacion = models.CharField(max_length=12, unique=True,
    validators=[
        RegexValidator(regex='^([0-9]{6,11})$', message='Identificación incorrecta.', code='invalid number')])
    nombres = models.CharField(max_length=100,validators=[
        RegexValidator(regex='[a-zA-Z][a-zA-Z ]+[a-zA-Z]$', message='Nombres Invalidos.', code='invalid text')])
    apellidos = models.CharField(max_length=100,validators=[
        RegexValidator(regex='[a-zA-Z][a-zA-Z ]+[a-zA-Z]$', message='Apellidos Invalidos.', code='invalid text')])

    class Meta:
        ordering = ['nombres', 'apellidos']

    def __str__(self):

        return str(self.identificacion+" "+self.nombres + " "+self.apellidos)

class Recurso(models.Model):
    categoria = models.CharField(max_length=50, choices=(
        ('Tecnologia', 'Tecnologia'), ('Salud', 'Salud'), ('Educación', 'Educación'),
        ('Aseo', 'Aseo'),('Alimentos', 'Alimentos'),('Otros', 'Otros')))
    codigo = models.CharField(max_length=100, unique=True)
    nombre = models.CharField(max_length=100,validators=[
        RegexValidator(regex='[a-zA-Z][a-zA-Z ]+[a-zA-Z]$', message='Nombre del producto invalido.', code='invalid text')])
    estado = models.CharField(max_length=100,default="Sin asignar")
    marca = models.CharField(max_length=100,validators=[
        RegexValidator(regex='[a-zA-Z][a-zA-Z ]+[a-zA-Z]$', message='Marca del producto invalida', code='invalid text')])
    serie = models.CharField(max_length=100, unique=True)

    class Meta:
        ordering = ['codigo', 'nombre', 'marca']

    def __str__(self):
        return self.nombre


class Asignacion(models.Model):
    id_persona = models.ForeignKey(Persona, blank=True, null=True)
    id_recurso = models.ForeignKey(Recurso, blank=True, null=True)

    def __str__(self):
        return str(self.id_recurso.nombre)

class Historial(models.Model):
    id_persona = models.ForeignKey(Persona, blank=True, null=True)
    id_recurso = models.ForeignKey(Recurso, blank=True, null=True)
    evento = models.CharField(max_length=100)
    fecha_asignacion = models.DateField(default=date.today)
